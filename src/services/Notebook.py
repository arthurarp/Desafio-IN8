
import time
import sys
from flask import jsonify
from functions.crawler import Crawler
import asyncio

crawler = Crawler()

class NotebookService:
  def find_all(self):
    try:
      notebooks_data = asyncio.run(crawler.main())
      filtered_data = []
      for notebook in notebooks_data:
        if 'lenovo' in notebook['title'].lower() or \
        'lenovo' in notebook['description'].lower():
          filtered_data.append(notebook)


      sorted_data = sorted(filtered_data, key=lambda k: k['price'], reverse=False)

      return jsonify(sorted_data)

    except Exception as e:
      print(e)
      return {
        'message': 'Erro ao buscar notebooks',
        'error': str(e)
      }, 500
   
 