import os
import sys
from flask_restx import Api

class Documentation:
  def __init__(self, app):
    self.app = app

  def create(self):
    return Api (
      self.app,
      doc="/api-docs",
      title="Aplicação teste: Desafio IN8 Crawler",
      version="1.0",
    )