import os
from flask import Flask
from doc.main import Documentation
from controllers.Notebook import notebook_route as NotebookRoutes

app = Flask(__name__)
doc = Documentation(app)
api = doc.create()

api.add_namespace(NotebookRoutes, path='/notebook')

if __name__ == '__main__':
   app.run(host='0.0.0.0', port=5000)