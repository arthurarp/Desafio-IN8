from flask import request
from flask_restx import Resource, Namespace
from services.Notebook import NotebookService

notebook_route = Namespace (
    "Notebook",
    description="Related to notebooks",
    validate=True,
)

service = NotebookService()

@notebook_route.route('/')
class Notebook(Resource):
  def get(self):
    " Esse endpoint retorna todos os notebooks lenovo encontrados no site alvo do scraping. Os dados são ordenados por ordem crescente de preço."
   
    return service.find_all()
