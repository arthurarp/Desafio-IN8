from playwright.async_api import async_playwright
import asyncio
class Crawler:
  def __init__(self):
    self.base_url = 'https://webscraper.io/'
    self.link ='https://webscraper.io/test-sites/e-commerce/allinone/computers/laptops'

  async def get_hdd_variations(self, relative_url):
    async with async_playwright() as pw:
      browser = await pw.chromium.launch()
      page = await browser.new_page()
      await page.goto(self.base_url + relative_url)

      base_locator = (
        page.locator('.thumbnail')
      )

      title = await base_locator.locator('.caption').locator('h4').nth(1).text_content()
      variations_list = []

      
      variations_container = await base_locator.locator('.swatches').locator('button').all()

      for variation in variations_container:
        await variation.click()
        variations_list.append({
          'hdd_capacity': await variation.text_content(),
          'price': await base_locator.locator('.price').text_content(),
        })
    

      await page.wait_for_timeout(1000)  # Wait for 1 second
      await browser.close()

      return title, variations_list

  async def main(self):
    async with async_playwright() as pw:
      browser = await pw.chromium.launch()
      page = await browser.new_page()
      await page.goto(self.link)

      base_locator = (
        page.locator('.thumbnail')
      )

      notebooks_data = []

      for notebook in await base_locator.all():
        # relative_url = await notebook.locator('a').get_attribute('href')
        # title, variations = await self.get_hdd_variations(relative_url)
        h4 = notebook.locator('.title')
        description = await notebook.locator('.description').nth(0).text_content(),
        price = await notebook.locator('.price').text_content()

        new_notebook = {
          'title': description[0].split(',')[0],
          'price': float(price[1:]),
          'description': description[0],
          'reviews': await notebook.locator('.ratings').locator('.pull-right').text_content(),
          'average_rating': await notebook.locator('.ratings').locator('p:nth-child(2)').get_attribute('data-rating'),

        }

        notebooks_data.append(new_notebook)
          
      await page.wait_for_timeout(1000)  # Wait for 1 second
      await browser.close()

      return notebooks_data

          
if __name__ == '__main__':
  craw = Crawler()
  asyncio.run(craw.main())