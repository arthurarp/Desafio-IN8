# Desafio-IN8

Etapa do processo seletivo para a vaga de desenvolvedor backend da empresa IN8.

# Como rodar

Para rodar o projeto é necessário ter o docker instalado. Então digite o seguinte comando:

    docker compose up --build

A documentação estará disponível em 

    http://localhost/api-docs
