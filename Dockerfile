FROM python:3.9

WORKDIR /crawler_challenge

COPY . .

RUN pip install -r requirements.txt

RUN playwright install

RUN playwright install-deps

RUN apt update

RUN apt install nano -y